# rocket-simulator

Simulates a rocket ship construction project.
An x86 64bit assembler program using nasm. 

![demo](https://gitlab.com/4669/rocket-simulator/raw/master/demo.webm)

# requirements

* x86 processor
* 64bit linux 
* nasm

# build

```
make rocket
```