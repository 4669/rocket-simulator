section .data 

   SYSTEM_EXIT      equ 1
   SYSTEM_WRITE     equ 4
   SYSTEM_READ      equ 3
   SYSTEM_NANOSLEEP equ 162
   STDIN  equ 0
   STDOUT equ 1
   CALL_KERNEL equ 80h
   DEFAULT_EXITCODE equ 0
   
   waitTime:
      tv_sec  dd 0 ;four bytes for nanosleep seconds
      tv_nsec dd 0 ;four bytes for nanosleep nanoseconds

   prompt db 'Please enter the length of Elon Musks rocket ship: ' 
   lenPrompt equ $-prompt

   dispMsg db 'The rocket ship is being built: ', 0xA,0xD 
   lenDispMsg equ $-dispMsg                 
   
   rocketShipBottom db '8'
   lenRocketShipBottom equ $-rocketShipBottom
   
   unfinishedRocketShipBottom db 'o'
   lenUnfinishedRocketShipBottom equ $-unfinishedRocketShipBottom
   
   rocketShipBase db '='
   lenRocketShipBase equ $-rocketShipBase
   
   unfinishedRocketShipBase db '-'
   lenUnfinishedRocketShipBase equ $-unfinishedRocketShipBase
   
   rocketShipTop db 'D'
   lenRocketShipTop equ $-rocketShipTop
   
   rocketShipSmoke db '~'
   lenRocketShipSmoke equ $-rocketShipSmoke
   
   backspace db 0x8, ' ', 0x8
   lenBackspace equ $-backspace
   
   newLine db 0xA
   lenNewLine equ $-newLine
   
   ;print string
   ; %1: string
   ; %2: length of string
   %macro print 2
      mov eax, SYSTEM_WRITE
      mov ebx, STDOUT
      mov ecx, %1
      mov edx, %2
      int      CALL_KERNEL
   %endmacro
   
   ;Read and store the user input
   ; %1: where to read
   ; %2: length of read
   %macro readUserInput 2
      mov eax, SYSTEM_READ
      mov ebx, STDIN
      mov ecx, %1
      mov edx, %2         
      int      CALL_KERNEL
   %endmacro
   
   ; use nanosleep provided by linux to wait
   ; %1: secs to wait
   ; %2: additional nanosecs to wait
   %macro waitAndSleep 2
      mov dword [tv_sec], %1
      mov dword [tv_nsec], %2
      mov eax, SYSTEM_NANOSLEEP
      mov ebx, waitTime
      mov ecx, 0
      int CALL_KERNEL
   %endmacro
   
section .bss           
   num resb 2

section .text 
   global _start

_start:

   print prompt, lenPrompt
   readUserInput num, 2
   
   sub byte [num], '0'
   
   print dispMsg, lenDispMsg

   waitAndSleep 1, 69
   
   print unfinishedRocketShipBottom, lenUnfinishedRocketShipBottom
   
   waitAndSleep 1, 0
   
   print unfinishedRocketShipBottom, lenUnfinishedRocketShipBottom
   
   waitAndSleep 1, 0
   
   print backspace, lenBackspace 
   print backspace, lenBackspace 
   print rocketShipBottom, lenRocketShipBottom

   ; loop building one base part
   printOneShipBase:
   
   waitAndSleep 5, 0
   
   print unfinishedRocketShipBase, lenUnfinishedRocketShipBase
   
   waitAndSleep 1, 0
   
   print backspace, lenBackspace
   print rocketShipBase, lenRocketShipBase
   
   dec byte [num]
   cmp byte [num], 0
   jg printOneShipBase ; while (num > 0) {
   
   print rocketShipTop, lenRocketShipTop
   waitAndSleep 1, 0
   print rocketShipSmoke, lenRocketShipSmoke
   waitAndSleep 1, 0
   print rocketShipSmoke, lenRocketShipSmoke
   waitAndSleep 0, 12345678
   print newLine, lenNewLine
    
   ; Exit program
   mov eax, SYSTEM_EXIT
   mov ebx, DEFAULT_EXITCODE
   int      CALL_KERNEL
   
